// Displays an Atlassian product's latest version, release notes and release day, as a row in a table.
// This callbackfunction is a JSONP handler for URLs such as https://my.atlassian.com/download/feeds/current/jira-core.json (see below).
function downloads(appData) {
	//console.log("downloads running");
	var url = appData[0].zipUrl;
	//console.log("Got zip URL " + url);
	var app = url.match(/\/atlassian-([^\d]+)-\d/)[1];
	//console.log("URL mentions app " + app);
	var appVer = appData.filter(function (e, i, arr) {
			return !e.description.match(/WAR/) && e.zipUrl.match(/.*\.(tar\.gz|zip)/);
			})[0].version;
	var released = appData[0].released.replace("-", " ", "g");
	var relNotes = appData[0].releaseNotes;
	//console.log("app version is " + appVer);
	$("#productTable").addClass("confluenceTable");
	if ($("#productTable tbody").length == 0) { $("#productTable").append("<tbody>"); }
	$("#productTable").append("<tr>" +
			"<th class='confluenceTh'>" + app + "</th>" + 
			"<td class='confluenceTd'><a href='" + relNotes + "'>" + appVer + "</a></td>" + 
			"<th class='confluenceTh'>Released</th>" + 
			"<td class='confluenceTd'>" + released + "</td></tr>");
}
// Get Atlassian to call our downloads() function with product info for each product.
// These URLs were inferred by poking around on the 'Downloads Archive' pages, e.g. https://www.atlassian.com/software/crowd/download-archive


$.getScript("https://my.atlassian.com/download/feeds/current/confluence.json");
$.getScript("https://my.atlassian.com/download/feeds/current/jira-core.json");
$.getScript("https://my.atlassian.com/download/feeds/current/jira-software.json");
$.getScript("https://my.atlassian.com/download/feeds/current/jira-servicedesk.json");
$.getScript("https://my.atlassian.com/download/feeds/current/fisheye.json");
$.getScript("https://my.atlassian.com/download/feeds/current/stash.json");
$.getScript("https://my.atlassian.com/download/feeds/current/bamboo.json");
$.getScript("https://my.atlassian.com/download/feeds/current/crowd.json");
